# Readme

## Desarrollo reproducción vídeo interactivo

### Casos de uso

#### Iniciar sesión grupal o Individual

Una sesión individual no requiere una página para las opciones ya
que se muestran en el mismo reproductor y se puede seleccionar desde
él.

Una sesión grupal requiere un nombre de sesión para poder diferenciarse de
otras sesiones y que los otros usuarios puedan identificarla fácilmente.

#### QR

Una vez generada una sesión se genera un QR hacía el enlace de la sesión,
ejemplo: papilio.com/nombre-de-sesion

#### Lista de sesiones activas

Se podría tener una página con la lista de las sesiones activas en caso
de ser públicas, en el caso de ser privadas solo se podría acceder desde
el QR o se podría requerir contraseña.

#### Opciones de reproducción

Todas las sesiones tendrán una url personalizada para cada sesión.
